﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentoUnico : MonoBehaviour {

	public float velocidade = 0f;
	public Renderer quad;

	// Use this for initialization
	void Start () {

		
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 offSet = new Vector2(velocidade * Time.deltaTime, 0);
		quad.material.mainTextureOffset += offSet;
	}
}
