﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atiradores : MonoBehaviour {

    public GameObject projetil;
    public GameObject origem;

    private GameObject projetilPai;

    private Animator animador;
    private Ressurgir ressurgirMinhaLinha;

	// Use this for initialization
	void Start () {

        animador = GameObject.FindObjectOfType<Animator>();

        projetilPai = GameObject.Find("Projetil");

        if (!projetilPai)
        {
            projetilPai = new GameObject();
            projetilPai.name = "Projetil";
        }

        RessurgirRaia();
		
	}
	
	// Update is called once per frame
	void Update () {

        if (AtacanteMesmaLinha())
        {
            animador.SetBool("Atacando", true);
        }
        else
        {
            animador.SetBool("Atacando", false);
        }
		
	}

    void RessurgirRaia()
    {
        Ressurgir[] colecaoRessurgir = GameObject.FindObjectsOfType<Ressurgir>();

        foreach (Ressurgir ressurgir in colecaoRessurgir)
        {
            if(ressurgir.transform.position.y == transform.position.y)
            {
                ressurgirMinhaLinha = ressurgir;
            }
        }
    }

    bool AtacanteMesmaLinha()
    {
        if (ressurgirMinhaLinha.transform.childCount <= 0)
        {
            return false;
        }

        foreach (Transform atacantes in ressurgirMinhaLinha.transform)
        {
            //Existem atacantes? se existirem, eles estão a frente? 
            if (atacantes.transform.position.x > transform.position.x)
            {
                return true;
            }
        }
        return false;
    }

    private GameObject Atirar()
    {
        GameObject novoProjetil = Instantiate(projetil) as GameObject;
        novoProjetil.transform.parent = projetilPai.transform;
        novoProjetil.transform.position = origem.transform.position;

        return novoProjetil;
    }
}
