﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ressurgir : MonoBehaviour {

    public GameObject[] atacantes;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        foreach(GameObject ressurgir in atacantes)
        {
            if(HoraDeRessurgir(ressurgir))
            {
                Surgir(ressurgir);
            }
        }
		
	}

    void Surgir(GameObject meuAtacante)
    {
        GameObject atacante = Instantiate(meuAtacante) as GameObject;
        atacante.transform.parent = transform;
        atacante.transform.position = transform.position;
    }

    bool HoraDeRessurgir(GameObject objetoAtacante)
    {
        Atacantes atacante = objetoAtacante.GetComponent<Atacantes>();

        float atrasoRessurgimento = atacante.vistoACada;
        float ressurgiPorSegundo = 1 / atrasoRessurgimento;

        float limite = ressurgiPorSegundo * Time.deltaTime;

        if (Random.value < limite)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
