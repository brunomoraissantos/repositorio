﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saude : MonoBehaviour {

    public float saude = 100f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CausarDano(float dano)
    {
        saude -= dano;

        if (saude<= 0)
        {
            DestruirPersonagem();
        }
    }

    private void DestruirPersonagem()
    {
        Destroy(gameObject);
    }
}
