﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Atacantes))]
public class Lagarto : MonoBehaviour {

    private Animator animador;
    private Atacantes atacantes;

	// Use this for initialization
	void Start () {

        animador = GetComponent<Animator>();
        atacantes = GetComponent<Atacantes>();		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D colisor)
    {
        GameObject objetoColidido = colisor.gameObject;

        if (!objetoColidido.GetComponent<Defensores>())
        {
            return;
        }

        if (objetoColidido.GetComponent<Pedra>())
        {
            Debug.Log("ENTRANDO NA COLISÃO DO LAGARTO!!!!");
            animador.SetBool("estaAtacando", true);
            atacantes.Atacar(objetoColidido);
        }
    }
}
