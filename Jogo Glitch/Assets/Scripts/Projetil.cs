﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projetil : MonoBehaviour {

    public float velocidade;

    public float dano;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate (Vector3.right * velocidade * Time.deltaTime);
	}

    void OnTriggerEnter2D(Collider2D colisor)
    {
        Saude saude = colisor.gameObject.GetComponent<Saude>();
        Atacantes atacante = colisor.gameObject.GetComponent<Atacantes>();

        if (saude && atacante)
        {
            saude.CausarDano(dano);
            Destroy(gameObject);
        }
    }
}
