﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GerenciadorNivel : MonoBehaviour {

	public float carregarProximoNivelAutomaticamente;

	void Start () 
	{
		if (carregarProximoNivelAutomaticamente <= 0) 
        {
            Debug.Log("Fazer Nada...");
		} 
        else 
        {
            Invoke ("CarregarProximoNivel", carregarProximoNivelAutomaticamente);
		}
	}
	

	void Update () 
	{
		
	}

	public void CarregarProximoNivel()
	{
		//Debug.Log("ENTROU NA FUNÇÂO!!!");
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void CarregarNivel(string nomeNivel)
	{
		SceneManager.LoadScene (nomeNivel);
	}
}
