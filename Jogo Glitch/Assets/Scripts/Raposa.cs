﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Atacantes))]
public class Raposa : MonoBehaviour {

    private Animator animador;
    private Atacantes atacante;

	// Use this for initialization
	void Start () {
		
        animador = GetComponent<Animator>();
        atacante = GetComponent<Atacantes>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D colisor)
    {
        GameObject objetoColidido = colisor.gameObject;

        if (!objetoColidido.GetComponent<Defensores>())
        {
            return;
        }

        if (objetoColidido.GetComponent<Pedra>())
        {
            animador.SetTrigger("Jump Trigger");
        }
        else
        {
            animador.SetBool("Atacando", true);
            Debug.Log("ENTRANDO NA COLISÃO DO RAPOSA!!!!");
            atacante.Atacar(objetoColidido);
            Debug.Log("ENTRANDO NA COLISÃO DO ATACANDO!!!!");
        }
    }
}
