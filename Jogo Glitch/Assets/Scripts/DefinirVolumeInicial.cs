﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefinirVolumeInicial : MonoBehaviour {

    private GerenciadorMusica gerenciador;

	// Use this for initialization
	void Awake () {
		
        gerenciador = GameObject.FindObjectOfType<GerenciadorMusica>();

        if (gerenciador)
        {
            float volume = PrefJogador.GetMasterVolume();
            gerenciador.AlterarVolume(volume);
        }
        else
        {
            Debug.LogWarning("Nenhum som encontrado");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
