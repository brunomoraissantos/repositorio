﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GerenciadorMusica : MonoBehaviour 
{
	public AudioClip[] listagemMusicas;

	private AudioSource audioSource;

	void Awake()
	{
		DontDestroyOnLoad (gameObject);
	}

	void Start () 
	{
		audioSource = GetComponent<AudioSource> ();
	}
	

	void Update () 
	{
		
	}

	void OnLevelWasLoaded(int nivel)
	{
		AudioClip musicaNivelAtual = listagemMusicas[nivel];

		if (musicaNivelAtual) 
		{
			audioSource.clip = musicaNivelAtual;
			audioSource.loop = true;
			audioSource.Play ();
		}
	}

    public void AlterarVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
