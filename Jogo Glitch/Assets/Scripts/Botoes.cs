﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botoes : MonoBehaviour {

    public GameObject prefabDefensor;
    public static GameObject defensorSelecionado;

    private Botoes[] colecaoBotoes;

	// Use this for initialization
	void Start () {
		
        colecaoBotoes = GameObject.FindObjectsOfType<Botoes>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        foreach (Botoes totalBotoes in colecaoBotoes)
        {
            totalBotoes.GetComponent<SpriteRenderer>().color = Color.black;
        }

        GetComponent<SpriteRenderer>().color = Color.white;

        defensorSelecionado = prefabDefensor;
    }
}
