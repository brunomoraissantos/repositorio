﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Transicao : MonoBehaviour {

    public float tempoTransicao;

    private Image painelTransicao;
    private Color corAtual = Color.black;

	// Use this for initialization
	void Start () {
        painelTransicao = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.timeSinceLevelLoad < tempoTransicao)
        {
            float mudancaAlfa = Time.deltaTime / tempoTransicao;
            corAtual.a -= mudancaAlfa;
            painelTransicao.color = corAtual;
        }
        else
        {
            gameObject.SetActive (false);
        }
		
	}
}
