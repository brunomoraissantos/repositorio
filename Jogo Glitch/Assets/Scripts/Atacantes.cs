﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class Atacantes : MonoBehaviour {

    public float vistoACada = 0f;

    private float velocidadeAtual = -1.5f;
    private GameObject alvoAtual;

    private Animator animador;


	// Use this for initialization
	void Start () {

        animador = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate (Vector3.left * velocidadeAtual * Time.deltaTime);
		
        if (!alvoAtual)
        {
            animador.SetBool("Atacando", false);
        }
	}

    void OnTriggerEnter2D()
    {
        
    }

    public void DefinirVelocidade(float velocidade)
    {
        velocidadeAtual = velocidade;
    }

    //chamado pelo animador na hora do ataque real
    public void AtingirAlvoAtual (float dano)
    {
        if (alvoAtual)
        {
            Saude saude = alvoAtual.GetComponent<Saude>();
            if (saude)
            {
                saude.CausarDano(dano);
            }
        }
    }

    public void Atacar (GameObject objeto)
    {
        alvoAtual = objeto;
        Debug.Log("EXISTE ALVO!!!!");
    }
}
