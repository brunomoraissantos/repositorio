﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RessurgirDefensores : MonoBehaviour {

    public Camera minhaCamera;

    private GameObject paiDefendor;

	// Use this for initialization
	void Start () {

        paiDefendor = GameObject.Find("Defensores");

        if (!paiDefendor)
        {
            paiDefendor = new GameObject("Defensores");
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        Vector2 posicaoInicial = CalcularPontosMundo();
        Vector2 posicaoArredondada = Arredondar(posicaoInicial);
        GameObject defensor = Botoes.defensorSelecionado;
        Quaternion rotacao = Quaternion.identity;
        GameObject novoDefensor = Instantiate(defensor, posicaoArredondada, rotacao) as GameObject;
        novoDefensor.transform.parent = paiDefendor.transform;
    }

    Vector2 CalcularPontosMundo()
    {
        float posicaoX = Input.mousePosition.x;
        float posicaoY = Input.mousePosition.y;
        float distanciaCamera = 10f;

        Vector3 vetoresCamera = new Vector3(posicaoX, posicaoY, distanciaCamera);

        Vector2 vetorFinal = minhaCamera.ScreenToWorldPoint(vetoresCamera);

        return vetorFinal;
    }

    Vector2 Arredondar(Vector2 numero)
    {
        float novaPosicaoX = Mathf.RoundToInt(numero.x);
        float novaPosicaoY = Mathf.RoundToInt(numero.y);

        return new Vector2(novaPosicaoX, novaPosicaoY);
    }
}
