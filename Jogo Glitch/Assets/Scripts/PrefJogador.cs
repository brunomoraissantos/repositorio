﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefJogador : MonoBehaviour 
{
    const string VOLUME_MESTRE = "volume_mestre";
    const string DIFICULDADE_MESTRE = "dificuldade_mestre";
    const string LIBERACAO_NIVEL = "nivel_desbloqueado";

    public static void SetMasterVolume(float volume)
    {
        if (volume > 0f && volume < 1f)
        {
            PlayerPrefs.SetFloat(VOLUME_MESTRE, volume);
        }
        else
        {
            Debug.LogError("Volume fora da curva");
        }
    }

    public static float GetMasterVolume()
    {
        Debug.Log("O volume é " +VOLUME_MESTRE);
        return PlayerPrefs.GetFloat(VOLUME_MESTRE);
    }

    public static void DesbloquearNivel(int nivel)
    {
        if (nivel <= Application.levelCount - 1)
        {
            PlayerPrefs.SetInt(LIBERACAO_NIVEL + nivel.ToString(), 1);
        }
        else
        {
            Debug.LogError("Nivel fora de ordem");
        }
    }

    public static bool NivelDesbloqueado(int nivel)
    {
        int valorNivel = PlayerPrefs.GetInt(LIBERACAO_NIVEL + nivel.ToString());
        bool nivelDesbloqueado = (valorNivel == 1);

        if (nivel <= Application.levelCount - 1)
        {
            return nivelDesbloqueado;
        }
        else
        {
            Debug.LogError("Nivel fora de ordem");
            return false;
        }
    }

    public static void SetDificuldade(float dificuldade)
    {
        if (dificuldade >= 1f && dificuldade <= 3f)
        {
            PlayerPrefs.SetFloat(DIFICULDADE_MESTRE, dificuldade);
        }
        else
        {
            Debug.LogError("dificuldade errada");
        }
    }

    public static float GetDificuldade()
    {
        
        return PlayerPrefs.GetFloat(DIFICULDADE_MESTRE);
    }
	
}
