﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorOpcoes : MonoBehaviour 
{
    public Slider volumeSlider;
    public Slider dificuldadeSlider;
    public GerenciadorNivel gerenciador;

    private GerenciadorMusica gerenciadorMusica;

	// Use this for initialization
	void Start () 
    {
        gerenciadorMusica = GameObject.FindObjectOfType<GerenciadorMusica>();
        volumeSlider.value = PrefJogador.GetMasterVolume();
        Debug.Log("O volume é " +volumeSlider.value);
        dificuldadeSlider.value = PrefJogador.GetDificuldade();
	}
	
	// Update is called once per frame
	void Update () 
    {
        gerenciadorMusica.AlterarVolume(volumeSlider.value);
	}

    public void SaveAndExit()
    {
        PrefJogador.SetMasterVolume(volumeSlider.value);

        PrefJogador.SetDificuldade(dificuldadeSlider.value);

        gerenciador.CarregarNivel("Start Menu");
    }

    public void SetDefault()
    {
        volumeSlider.value = 0.8f;
        dificuldadeSlider.value = 2f;
    }
}
